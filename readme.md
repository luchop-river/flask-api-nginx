# Crear y correr docker
Correr el siguiente comando dentro del mismo directorio que *docker-compose.yml*
para construir los servicios
`docker-compose build`
Luego creamos y arrancamos los containers
`docker-compose up`
Opcionalmente el siguiente comando permite realizar ambas cosas
`docker-compose up --build`

