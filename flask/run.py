from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import ModelSchema
from flask import jsonify
from flask import request
from datetime import datetime
import dateutil.parser


app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)


# Modelos

class Doctor(db.Model):
    __tablename__ = 'doctores'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(64),nullable=False)
    apellido = db.Column(db.String(64),nullable=False)
    dni = db.Column(db.Integer,nullable=False,unique=True)
    matricula = db.Column(db.Integer,nullable=False,unique=True)
    especialidad = db.Column(db.String(256))
    # OneToMany: un doctor hace muchas consultas
    consultas = db.relationship('Consulta')

class Enfermedad(db.Model):
    __tablename__ = 'enfermedades'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(256),nullable=False)

enfermedades_pacientes_table = db.Table('enfermedades_pacientes', db.Model.metadata,
    db.Column('enfermedad_id', db.Integer, db.ForeignKey('enfermedades.id')),
    db.Column('paciente_id', db.Integer, db.ForeignKey('pacientes.id'))
)

class Paciente(db.Model):
    __tablename__ = 'pacientes'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(64),nullable=False)
    apellido = db.Column(db.String(64),nullable=False)
    dni = db.Column(db.Integer,nullable=False,unique=True)
    fecha_nacimiento = db.Column(db.DateTime)
    enfermedades = db.relationship('Enfermedad',
        secondary=enfermedades_pacientes_table)
    # OneToMany: un paciente tiene muchas consultas
    consultas = db.relationship('Consulta')


class Consulta(db.Model):
    __tablename__ = 'consultas'
    id = db.Column(db.Integer, primary_key=True)
    diagnostico = db.Column(db.String(512))
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    # una consulta es hecha por un doctor
    doctor_id = db.Column(db.Integer,db.ForeignKey('doctores.id'))
    # una consulta es hecha para un paciente
    paciente_id = db.Column(db.Integer,db.ForeignKey('pacientes.id'))


# Esquemas: marshmallow

class DoctorSchema(ma.ModelSchema):
    class Meta:
        model = Doctor

class EnfermedadSchema(ma.ModelSchema):
    class Meta:
        model = Enfermedad

class PacienteSchema(ma.ModelSchema):
    class Meta:
        model = Paciente


class ConsultaSchema(ma.ModelSchema):
    class Meta:
        model = Consulta


db.create_all()


# API

def _close_and_remove_session():
    """
    Cierra y elimina la sesion de la base.    
    """
    db.session.close()
    db.session.remove()


def _flush_and_rollback_session():
    """
    Realiza un flush y un rollback en la sesion.
    """
    db.session.flush()
    db.session.rollback()


def _commit_changes(msg_success,msg_fail):
    """
    Realiza un commit en la sesion de la base.
    Retorna el mensaje de exito o fracaso que recibe al final
    """
    try:
        db.session.commit()
        return msg_success
    except:        
        _flush_and_rollback_session()
        return msg_fail
    finally:
        _close_and_remove_session


def _add_and_commit_changes(un_objeto,msg_success,msg_fail):
    """
    Agrega un objeto y luego realiza el commit en la base
    Retorna el mensaje de exito o fracaso que recibe al final
    """
    try:
        db.session.add(un_objeto)        
    except:        
        _close_and_remove_session
        return msg_fail
    return _commit_changes(msg_success,msg_fail)


def _es_mensaje(un_objeto):
    """
    Retorna un booleano dependiendo si el 
    """
    return isinstance(un_objeto,str)


# Listar / Crear Doctores

@app.route('/api/doctor', methods = ['GET'])
def get_doctores():
    """ 
    Retorna todos los doctores que hay en la base de datos
    En caso de exito retorna un JSON con la lista de todos los doctores
    En caso de falla retorna un mensaje
    """
    try:
        result_doctores = Doctor.query.all()
        doctor_schema = DoctorSchema(many=True)
        doctores = doctor_schema.dump(result_doctores)
        return jsonify({'doctores':doctores})
    except:
        return '500. Hubo un error devolviendo doctores\n'


@app.route('/api/doctor', methods = ['POST'])
def create_doctor():
    """ 
    Crea un nuevo doctor y lo agrefa a la base de datos
    En caso de exito o falla retorna un mensaje indicando el mismo
    """
    new_doctor = _get_new_doctor_or_msg(request.get_json(force=True))
    if _es_mensaje(new_doctor):
        return new_doctor
    else:        
        msg_success = '201. Doctor guardado exitosamente!\n'
        msg_fail = '500. Error al crear el doctor\n'
        return _add_and_commit_changes(new_doctor,msg_success,msg_fail)


def _get_new_doctor_or_msg(request_json_format):
    """ 
    Crea un un nuevo doctor
    En caso de exito retorna el doctor creado a traves del json que recibe
    En caso de falla retorna un mensaje
    """
    try:
        new_doctor = Doctor()
        new_doctor.nombre = request_json_format['nombre']
        new_doctor.apellido = request_json_format['apellido']
        new_doctor.dni = request_json_format['dni']
        new_doctor.matricula = request_json_format['matricula']
        new_doctor.especialidad = request_json_format['especialidad']        
        return new_doctor
    except:
        return '400. Formato incorrecto\n'


# Listar / Crer enfermedades

@app.route('/api/enfermedades', methods = ['GET'])
def get_enfermedades():
    """ 
    Retorna las enfermedades que existan en la BD o un 
    mensaje si no pudo hacerlo
    """
    try:
        result_enfermedades = Enfermedad.query.all()
        enfermedad_schema = EnfermedadSchema(many=True)
        enfermedades = enfermedad_schema.dump(result_enfermedades)
        return jsonify({'enfermedades':enfermedades})
    except:
        return '500. Hubo un error devolviendo enfermedades\n'


@app.route('/api/enfermedades', methods = ['POST'])
def create_enfermedad():
    """
    Permite crear una enfermedad en la Base de Datos.
    Recive por metodo POST una descripcion de tipo texto
    """
    new_enfermedad = _get_new_enfermedad_or_msg(request.get_json(force=True))
    if _es_mensaje(new_enfermedad):
        return new_enfermedad
    else:        
        msg_success = '201. Enfermedad guardada exitosamente!\n'
        msg_fail = '500. Error al crear la enfermedad\n'
        return _add_and_commit_changes(new_enfermedad,msg_success,msg_fail)


def _get_new_enfermedad_or_msg(request_json_format):
    """
    Crea y retorna una enfermedad con las descripcion recinida en formato JSON
    En caso de error retorna un mensaje
    """
    try:
        return Enfermedad(descripcion = request_json_format['descripcion'])        
    except:
        return '400. Formato incorrecto\n'


# Listar / Crear / Actualizar Consulta(s)

@app.route('/api/doctor/<int:id_doctor>/consultas', methods = ['GET'])
def get_consultas(id_doctor):
    """
    Retorna todas las consultas realizadas por el doctor en cuestion
    """
    if _existe_id_doctor(id_doctor):
        result_consultas = Consulta.query.filter_by(doctor_id = id_doctor).all()
        consulta_schema = ConsultaSchema(many=True)
        consultas = consulta_schema.dump(result_consultas)
        return jsonify({'consultas':consultas})
    else:
        return f'404. No existe el doctor con id {id_doctor}\n'


@app.route('/api/doctor/<int:id_doctor>/consultas/<int:id_consulta>', methods = ['GET'])
def get_consulta(id_doctor,id_consulta):
    """
    Retorna la consulta deseada que realizo el doctor. El id_doctor e id_consulta deben ser validos
    """
    if _existe_id_doctor(id_doctor) and _existe_id_consulta(id_consulta):
        result_consulta = Consulta.query.filter_by(doctor_id = id_doctor,id=id_consulta).first()
        if result_consulta == None:
            return f'404. El doctor con id {id_doctor} no realizo la consulta {id_consulta}\n'
        else:
            consulta_schema = ConsultaSchema()
            consulta = consulta_schema.dump(result_consulta)
            return jsonify({'consulta':consulta})
    else:
        return f'404. No existe el doctor con id {id_doctor} o la consulta {id_consulta}\n'


@app.route('/api/doctor/<int:id_doctor>/consultas/<int:id_consulta>', methods = ['POST'])
def update_diagnostico_de_consulta(id_doctor,id_consulta):
    """
    Actualiza el diagnostico de una consulta
    """
    if _existe_id_doctor(id_doctor) and _existe_id_consulta(id_consulta):
        consulta = Consulta.query.filter_by(doctor_id = id_doctor,id=id_consulta).first()
        new_consulta = _update_diagnistico_or_msg(consulta,request.get_json(force=True))
        if _es_mensaje(new_consulta):
            return new_consulta
        else:
            msg_success = '204. Consulta modificada exitosamente!\n'
            msg_fail = '500. Error al modificar la consulta\n'
            return _commit_changes(msg_success,msg_fail)        
    else:
        return f'404. No existe el doctor con id {id_doctor} o la consulta {id_consulta}\n'


def _update_diagnistico_or_msg(una_consulta,request_json_format):
    """
    Recibe una consulta y le actualiza el diagnostico con el json que recibe.
    En caso de error retorna un mensaje
    """
    try:
        una_consulta.diagnostico = request_json_format['diagnostico']
        return una_consulta
    except:
        return '400. Formato incorrecto\n'


@app.route('/api/doctor/<int:id_doctor>/consultas', methods = ['POST'])
def create_consulta(id_doctor):
    """
    Permite crear una nueva consulta: 
    Necesita diagnostico & id_paciente en el JSON que reciba
    """
    if _existe_id_doctor(id_doctor):
        new_consulta = _get_new_consulta_or_msg(id_doctor,request.get_json(force=True))
        if _es_mensaje(new_consulta):
            return consulta
        else:
            msg_success = '201. Consulta guardada exitosamente!\n'
            msg_fail = '500. Error al crear la consulta\n'
            return _commit_new_consulta(new_consulta,msg_success,msg_fail)            
    else:
        return f'404. No existe el doctor con id {id_doctor}\n'


def _commit_new_consulta(new_consulta,msg_success,msg_fail):
    """
    Agrega y commitea una nueva consulta en la base, junto con el doctor y
    paciente, que poseen referencias a ella
    """
    try:
        if msg_success == _add_and_commit_changes(new_consulta, msg_success,msg_fail):            
            # pude guardar la nueva consulta
            doctor = Doctor.query.get(new_consulta.id_doctor)
            doctor.consultas.append(new_consulta)
            if msg_success == _commit_changes(msg_success,msg_fail):                
                # puede guardar consulta en doctor
                paciente = Paciente.query.get(new_consulta.id_paciente)                
                paciente.consultas.append(new_consulta)
                # prebo guardar la consulta en el paciente          
                return _commit_changes(msg_success,msg_fail)
        return msg_fail
    except:
        _flush_and_rollback_session()
        _close_and_remove_session
        return msg_fail


def _existe_id_doctor(id_doctor):
    """
    Retorna un booleano indicando si existe o no el doctor con el id indicado
    """
    return None != Doctor.query.get(id_doctor)


def _existe_id_consulta(id_consulta):
    """
    Retorna un booleano indicando si existe o no la consulta con el id indicado
    """
    return None != Consulta.query.get(id_consulta)


def _get_new_consulta_or_msg(id_doctor,request_json_format):
    """
    Retorna una nueva consulta o un mensaje si no pudo crearla
    Recibe el id del doctor y un JSON con la descripcion de la consulta 
    junto con el id del paciente
    """
    try:
        new_consulta = Consulta()
        new_consulta.diagnostico = request_json_format['diagnostico']
        new_consulta.id_doctor = id_doctor
        new_consulta.id_paciente = request_json_format['id_paciente']
        return new_consulta
    except Exception as error:
        return '400. Formato incorrecto\n'


# Listar / Crear Paciente(s)

@app.route('/api/pacientes', methods = ['GET'])
def get_pacientes():
    """
    Retorna la lista de pacientes que hay en la base
    """
    result_pacientes = Paciente.query.all()
    paciente_schema = PacienteSchema(many=True)
    pacientes = paciente_schema.dump(result_pacientes)
    return jsonify({'pacientes':pacientes})


@app.route('/api/pacientes/<int:id_paciente>', methods = ['GET'])
def get_paciente(id_paciente):
    """
    Retorno un unico paciente. Para ello necesito el id
    """
    if _existe_id_paciente(id_paciente):
        result_paciente = Paciente.query.get(id_paciente)
        paciente_schema = PacienteSchema()
        paciente = paciente_schema.dump(result_paciente)
        return jsonify({'paciente':paciente})
    else:
        return f'404. No existe el paciente con id {id_paciente}\n'

@app.route('/api/pacientes', methods = ['POST'])
def create_paciente():
    """
    Crea y devuelve un nuevo paciente con los datos que recibe en formato JSON
    En caso de error retorna un mensaje
    """
    request_json_format = request.get_json(force=True)
    paciente = _get_new_paciente_or_msg(request_json_format)
    if _es_mensaje(paciente):
        return paciente
    else:
        msg_success = '201. Paciente guardado exitosamente!\n'
        msg_fail = '500. Error al crear el paciente\n'
        return _add_and_commit_changes(paciente,msg_success,msg_fail)


def _get_new_paciente_or_msg(request_json_format):
    """
    Crea y devuelve un paciente con los datos que recibe en formato JSON.
    En caso de error retorna un mensaje
    """
    try:
        new_paciente = Paciente()
        new_paciente.nombre = request_json_format['nombre']
        new_paciente.apellido = request_json_format['apellido']
        new_paciente.dni = request_json_format['dni']
        new_paciente.fecha_nacimiento =dateutil.parser.parse(request_json_format['fecha_nacimiento'])
        return new_paciente
    except Exception as error:
        return '400. Formato incorrecto\n' + str(error)


def _existe_id_paciente(id_paciente):
    """
    Retorna un booleano indicando si existe el paciente en la base
    """
    return None != Paciente.query.get(id_paciente)


# Listar consulas del paciente

@app.route('/api/pacientes/<int:id_paciente>/consultas', methods = ['GET'])
def get_consultas_paciente(id_paciente):
    """
    Lista las consultas del paciente
    """
    if _existe_id_paciente(id_paciente):
        result_consultas = Consulta.query.filter_by(paciente_id = id_paciente).all()
        consulta_schema = ConsultaSchema(many=True)
        consultas = consulta_schema.dump(result_consultas)
        return jsonify({'consultas':consultas})
    else:
        return f'404. No existe el paciente con id {id_paciente}'


# Listar / Actualizar enfermedades del paciente

@app.route('/api/pacientes/<int:id_paciente>/enfermedades', methods = ['GET'])
def get_enfermedades_paciente(id_paciente):
    """
    Lista las enfermedades del paciente
    """
    if _existe_id_paciente(id_paciente):
        paciente = Paciente.query.get(id_paciente)
        result_enfermedades = list(map(lambda x : Enfermedad.query.get(x.id),paciente.enfermedades))
        enfermedad_schema = EnfermedadSchema(many=True)
        enfermedades = enfermedad_schema.dump(result_enfermedades)
        return jsonify({'enfermedades':enfermedades})
    else:
        return f'404. No existe el paciente con id {id_paciente}'


@app.route('/api/pacientes/<int:id_paciente>/enfermedades', methods = ['POST'])
def update_enfermedades_paciente(id_paciente):
    """
    Actualizo la lista de enfermedades del paciente. Recibe enfermedades
    """
    if _existe_id_paciente(id_paciente):
        paciente = Paciente.query.get(id_paciente)        
        paciente = _update_enferemedades_or_msg(paciente,request.get_json(force=True))        
        if _es_mensaje(paciente):
            return paciente
        else:
            msg_success = '204. Enfermedad(es) del paciente modificada(s) exitosamente!\n'
            msg_fail = '500. Error al modificar las enfermedades del paciente\n'
            return _commit_changes(msg_success,msg_fail)        
    else:
        return f'404. No existe el paciente con id {id_paciente}'


def _update_enferemedades_or_msg(un_paciente,request_json_format):
    """
    Recibe paciente, al cual le asigna las enfermedades que recibe
    """
    try:
        un_paciente.enfermedades.clear()
        identificadores_de_enfermedades = request_json_format['enfermedades']
        lista_enfermedades = _get_lista_enfermedades(identificadores_de_enfermedades)
        for enfermedad in lista_enfermedades:
            un_paciente.enfermedades.append(enfermedad)
        return un_paciente
    except:
        return '400. Formato incorrecto\n'


def _get_lista_enfermedades(identificadores_de_enfermedades):
    """
    Retorna una lista de las enfermeades de los identificadores que recibe
    """
    return list(map(lambda e:Enfermedad.query.get(e), identificadores_de_enfermedades))


if __name__ == "__main__":
    app.run(debug=True)

