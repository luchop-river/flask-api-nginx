# API: Clinica
Le permite a un profesional gestionar las consultas médicas que realiza y 
actualizar el esatado de salud de los pacientes.

# Modelos
## Doctor
+ nombre
+ apellido
+ dni
+ matricula
+ especialidad
+ consultas
## Enfermedad
+ descripcion
## Paciente
+ nombre
+ apellido
+ dni
+ fecha_nacimiento
+ enfermedades
+ consultas
## Consultas
+ diagnostico
+ doctor_id
+ paciente_id

# Ejemplos de uso

## Listar / Crear Doctores

**Listar Doctores**
```
curl -X GET http://localhost/api/doctor

```

**Crear un doctor**
*Debe recibir los siguientes campos: nombre, apellido, dni y especialidad*
```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/doctor -d \
'{"nombre":"dr nick","apellido":"desconocido","dni":22321123,"matricula":99999,"especialidad":"cosas"}'

```
```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/doctor -d \
'{"nombre":"desconocido","apellido":"hibbert","dni":29831323,"matricula":45851,"especialidad":"pediatra"}'
```

## Listar / Crer enfermedades


**Listar enfermedades**
```
curl -X GET http://localhost/api/enfermedades
```

**Crear una enfermedad**
*Debe recibir el campo: descripcion*
```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/enfermedades -d '{"descripcion":"conjuntivitis"}'
```
```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/enfermedades -d '{"descripcion":"resfrio"}'
```
## Listar / Crear Paciente(s)

**Listar Pacientes** 
```
curl -i -X GET -H "Content-Type:application/json" http://localhost/api/pacientes
```

**Traer un paciente especifico**
```
curl -i -X GET -H "Content-Type:application/json" http://localhost/api/pacientes/1
```

**Crear Paciente** 
*Debe recibir los siguientes campos: nombre, apellido, dni y fecha_nacimiento*


```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/pacientes -d \
'{"nombre":"Carl","apellido":"Carlson","dni":10876123,"fecha_nacimiento":"1988-04-23T18:25:43.511Z"}'
```
*(Fecha formato js)*
```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/pacientes -d \
'{"nombre":"lenny","apellido":"leonard","dni":21675123,"fecha_nacimiento":"2012-04-21T18:25:43-05:00"}'
```
*(Fecha formato JSON)*

# Listar / Crear / Actualizar Consulta(s)

**Listar consultas de un doctor**
```
curl -i -X GET  http://127.0.0.1/api/doctor/1/consultas
```

**Crear una consulta**
*Debe recibir los siguientes campos: diagnostico e id_paciente*
```
curl -i -X POST -H "Content-Type:application/json"  http://127.0.0.1/api/doctor/1/consultas -d '{"diagnostico":"debe descansar","id_paciente":1}'
```
```
curl -i -X POST -H "Content-Type:application/json"  http://127.0.0.1/api/doctor/2/consultas -d '{"diagnostico":"esta resfriado","id_paciente":1}'
```

**Traer una consulta especifica**
```
curl -i -X GET  http://127.0.0.1/api/doctor/1/consultas/1
```

**Modificar consulta**
*Debe recibir el campo: diagnostico*
```
curl -i -X POST -H "Content-Type:application/json" http://localhost/api/doctor/1/consultas/1 -d '{"diagnostico":"no tiene nada"}'
```


## Listar consulas del paciente

**Listar consultas del paciente**
```
curl -i -X GET -H "Content-Type:application/json" http://localhost/api/pacientes/1/consultas
```
## Listar / Actualizar las enfermeades del paciente

**Lista las enfermedades del paciente**
```
curl -i -X GET -H "Content-Type:application/json" http://localhost/api/pacientes/1/enfermedades
```
**Update Enfermedades Paciente**
*Debe recibir el campo: enfermedades*
```
curl -i -X POST -H "Content-Type:application/json"  http://127.0.0.1/api/pacientes/1/enfermedades -d '{"enfermedades":[1,2]}'
```
